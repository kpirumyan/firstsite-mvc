// prev, next и delete event handlers on Gallery page
var img = document.getElementById('img_n');

var del = document.getElementById('del');
del.onclick = function(){
	var hidden_img = document.getElementById('hidden_img');	
	var hidden_field = document.getElementById('hidden_field');				
	hidden_img.setAttribute('value',img.getAttribute('src'));
	hidden_field.setAttribute('value','delete');
};

var prev = document.getElementById('prev');
prev.onclick = function(){
	var hidden_img = document.getElementById('hidden_img');	
	var hidden_field = document.getElementById('hidden_field');		
	hidden_img.setAttribute('value',img.getAttribute('src'));
	hidden_field.setAttribute('value','prev');
};

var next = document.getElementById('next');
next.onclick = function(){
	var hidden_img = document.getElementById('hidden_img');	
	var hidden_field = document.getElementById('hidden_field');		
	hidden_img.setAttribute('value',img.getAttribute('src'));
	hidden_field.setAttribute('value','next');
};