<?php
/**
 * The Main menu configuration array.
 * $menu_items = [
 *      'item_name'     => [
 *      	'title' => (string) Menu link title .Required.
 *      	'path'     => (string) Menu link URL. Required.
 *      	'access'     => (integer) Access flag. 0 - public, 1 - protected, -1 - public only. Optional, default 0.
 *      ]
 *    ]
 */
	$main_menu = array(
		'main' => array(
			'title' => "Biography",
			'path' => "main",
			'access' => 0
		),
		'gallery' => array(
			'title' => "Gallery",
			'path' => "gallery",
			'access' => 0
		),
		'skills' => array(
			'title' => "Skills",
			'path' => "skills",
			'access' => 0
		),
		'contacts' => array(
			'title' => "Contacts",
			'path' => "contacts",
			'access' => 0
		), 
		'feedback' => array(
			'title' => "Feedback",
			'path' => "feedback/feedback",
			'access' => 0
		),
		'messages' => array(
			'title' => "Messages",
			'path' => "feedback/messages",
			'access' => 0
		),
		'LogIn' => array(
			'title' => "LogIn",
			'path' => "user/log_in",
			'access' => -1
		),
		'LogOut' => array(
			'title' => "LogOut",
			'path' => "user/log_out",
			'access' => 1
		),
	);
?>