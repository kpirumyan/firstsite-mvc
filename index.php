<?php
	ini_set('display_errors', 1);

	require_once 'application/core/view.php';
	require_once 'application/core/controller.php';
	require_once 'application/core/route.php';	
	require_once 'menu_config.php';	
	require_once 'application/core/menu.php';
	
	// get site directory
	$site_dir = explode('/', $_SERVER['PHP_SELF'])[1];
	$generatedMenu; 
	
	Route::start(); 
?>