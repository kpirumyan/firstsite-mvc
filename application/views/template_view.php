<?php
	global $site_dir;
	global $generatedMenu;
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<title>Karen Pirumyan - <?php echo $header ?></title>	
	<?php		
		echo "<link href='/$site_dir/css/styleSheet.css' rel='stylesheet' />";
	?>
</head>
<body>
  <header>Karen Pirumyan - <?php echo $header ?></header>
	<div id="navbar">
		<nav>
			<ul>
				<?php 					
					foreach ($generatedMenu as $path => $title) {
						echo "<li><a href='/{$site_dir}/{$path}'>{$title}</a></li>";
					}
				?>
			</ul>
		</nav>
		<hr /> 
		<div id="content">
			<?php echo $content_view; ?>
		</div	
	</div>
</body>
</html>     
       
