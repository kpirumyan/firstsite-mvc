<strong>LANS LLC</strong><br /><br />
<i>Specialization of the company:</i><br /> 
Production (through assembly) and sale of computer equipment, software and accessories, repair and service support of computer equipment, design and installation of local and global networks.
<br /><br />
<i>Work period:</i><br />
2009 - up to now
<br /><br />
<i>Position:</i><br />
Engineer of the service department (2009-2011)<br />
System Administrator (2012 - up to now)<br />
Head of the Authorized Service Center (ASC) Lenovo (2013-2016)<br /><br />
<i>Functional responsibilities:</i><br />
<ins>Engineer:</ins>
<ul>
	<li>Diagnostics and repair of laptops, PCs and tablets;</li>
	<li>Installation of OS, flashing tablets, removal of viruses;</li>
</ul>
<ins>System Administrator:</ins>
<ul>
	<li>installation, configuration, optimization of system software in the enterprise;</li>
	<li>creation of backup copies of data, data recovery;</li>
	<li>implementation of connection, testing, preventive maintenance, repair, modernization of computer facilities;</li>
	<li>provision of anti-virus protection for PCs;</li>
	<li>connection and setting up of modems, office PBX;</li>
	<li>support for the official site;</li>
	<li>administration of networks of partner companies;</li>
</ul>
<ins>Head of the Lenovo Service Center:</ins>
<ul>
	<li>Work with clients and partners;</li>
	<li>Delivery monthly reports on Lenovo warranty repairs, as well as expense reports;</li>
	<li>Interaction with Lenovo technical support;</li>
	<li>Conferences with the regional curator and other ASCs, exchange of experience.</li>
</ul>
<br />
<strong>MILITARY SERVICE</strong><br /><br />
2006 - 2008	Hadrut, Nagorno-Karabakh Republic.<br />
Sanitary therapist, paramedic
<br /><br /><br />
<strong>EDUCATION</strong><br /><br />
1998 - 2004	Russian State Medical University<br />
Medical Faculty<br />
Specialty: doctor<br />
Specialization: medical business<br /><br />
2011-2017	Participation in seminars from companies Microsoft, Kaspersky, Lenovo, Dell, Jabra  
