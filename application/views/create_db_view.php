<h2>Configuration settings</h2><br>
<form method='POST' id='config-form' action='create_db/post_index'>
	<div>
		<label for='host'>Host:</label>
		<input type='text' id='host' name='host' required >
	</div>
	<div>
		<label for='user'>Username:</label>
		<input type='text' id='login' name='login' required >
	</div>
	<div>
		<label for='password'>Password:</label>
		<input type='password' id='password' name='password' required >
	</div>
	<div>
		<label for='db-name'>DataBase Name:</label>
		<input type='text' id='db-name' name='db-name' required >
	</div>
	<div>
		<label for='admin-pass'>Admin password:</label>
		<input type='password' id='admin-pass' name='admin-pass' required >
	</div>
	<br>
	<input type='hidden' id='hid_field' name='hid_field' value=<?php echo $data ?>>
	<input type='submit'>
</form>
