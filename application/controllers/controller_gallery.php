<?php
	class Controller_Gallery extends Controller
	{
		public function __construct(){
			global $generatedMenu;
			$generatedMenu = Menu::generate_menu();
			$this->model = new Model_Gallery();
			parent::__construct();
		}
		
		public function index(){
			$this->view->generate('gallery_view.php', 'template_view.php', 'Gallery', $this->model);
		}	
	}
?>