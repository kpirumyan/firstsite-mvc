<?php
	class Controller_Main extends Controller
	{
		public function __construct(){
			global $generatedMenu;
			$generatedMenu = Menu::generate_menu();
			parent::__construct();
		}
		
		public function index(){	
			$this->view->generate('main_view.php', 'template_view.php', 'Biography');
		}
	}
?>