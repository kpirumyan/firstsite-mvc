<?php
	class Controller_Feedback extends Controller
	{
		private $model;
		
		public function __construct(){
			global $generatedMenu;
			$generatedMenu = Menu::generate_menu();
			$this->model = new Model_Feedback();
			parent::__construct();
		}
		
		public function feedback(){
			if (self::is_config_exist()) {
				$this->show_feedback();
			}
			else {
				header("Location: /" . $GLOBALS['site_dir'] . "/create_db?referer=feedback/feedback");
				exit();
			}
		}	

		public function post_feedback(){
			$data = $this->model->insert($_POST['name'], $_POST['email'], $_POST['post']);			
			$this->view->generate('feedback_view.php', 'template_view.php', 'Feedback', $data);
		}	
		
		private function show_feedback(){
			$this->view->generate('feedback_view.php', 'template_view.php', 'Feedback');
		}
		
		public function messages(){
			if (self::is_config_exist()) {
				$this->show_messages();
			}
			else {
				header("Location: /" . $GLOBALS['site_dir'] . "/create_db?referer=feedback/messages");
				exit();
			}
		}	
		
		private function show_messages(){
			if (self::is_authorized()) {
				$this->view->generate('messages_view.php', 'template_view.php', 'Messages', $this->model->get_list());
			}
			else {
				header("Location: /" . $GLOBALS['site_dir'] . "/user/log_in?referer=feedback/messages");
				exit();
			}
		}
		
		public function response(){
			$this->view->generate('response_view.php', 'template_view.php', 'Response', $this->model->get_post_byID($_GET['id']));
		}	
		
		public function remove(){
			$this->model->remove_post($_GET['id']);
			$this->view->generate('messages_view.php', 'template_view.php', 'Response', $this->model->get_list());
		}	
	}
	
?>