<?php
	require_once "/application/core/user_config.php";

	class Controller_Create_DB extends Controller
	{
		private $model;
		public function __construct(){
			global $generatedMenu;
			$generatedMenu = Menu::generate_menu();
			$this->model = new Model_Create_DB();
			parent::__construct();
		}
		
		public function index(){
			$this->view->generate('create_db_view.php', 'template_view.php', 'Configuration Settings', $_GET['referer']);
		}
		
		public function post_index(){
			$uc = new UserConfig();
			$this->model->get_form_data($uc);
			$this->model->create_db($uc);
			$this->model->create_config_file($uc);
			
			header("Location: /" . $GLOBALS['site_dir'] . '/' . htmlentities($_POST['hid_field']));
			exit();
		}	
	}
?>