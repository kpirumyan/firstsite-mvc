<?php
	class Controller_Upload extends Controller
	{
		function __construct(){
			global $generatedMenu;
			$generatedMenu = Menu::generate_menu();
			$this->model = new Model_Upload();
			parent::__construct();	
		}
			
		function index(){	
			if ($_FILES) {
				// Uploading images on server
				$this->model->upload_images($_FILES);
				
				// Getting upload status			
				foreach ($_FILES['uploads']['error'] as $key => $error) {
					$this->model->image_error($error);
				}
			}
			
			$this->view->generate('upload_view.php', 'template_view.php', 'Upload', $this->model);
		}
	}
?>