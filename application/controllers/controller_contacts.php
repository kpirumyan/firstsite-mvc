<?php
	class Controller_Contacts extends Controller
	{	
		public function __construct(){
			global $generatedMenu;
			$generatedMenu = Menu::generate_menu();
			parent::__construct();
		}
		
		public function index(){	
			$this->view->generate('contacts_view.php', 'template_view.php', 'Contacts');
		}
	}
?>