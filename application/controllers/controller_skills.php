<?php
	class Controller_Skills extends Controller
	{
		public function __construct(){
			global $generatedMenu;
			$generatedMenu = Menu::generate_menu();
			parent::__construct();
		}
		
		public function index(){	
			$this->view->generate('skills_view.php', 'template_view.php', 'Skills');
		}
	}
?>