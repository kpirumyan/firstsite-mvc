<?php
	class Controller_User extends Controller
	{
		private $model;
		private $data = array();
		
		public function __construct(){
			global $generatedMenu;
			$generatedMenu = Menu::generate_menu();
			$this->model = new Model_User();
			parent::__construct();
		}
		
		public function log_in(){
			if (self::is_config_exist()) {
				if (isset($_GET['referer'])) $this->data['referer'] = $_GET['referer'];
				$this->view->generate('login_view.php', 'template_view.php', 'Authorization', $this->data);
			}
			else {
				header("Location: /" . $GLOBALS['site_dir'] . "/create_db?referer=user/log_in");
				exit();
			}			
		}		
		
		public function log_out(){
			if (self::is_config_exist()) {
				$this->view->generate('exit_view.php', 'template_view.php', 'Exit');
			}
			else {
				header("Location: /" . $GLOBALS['site_dir'] . "/create_db?referer=user/log_in");
				exit();
			}	
		}	
		
		public function post_log_in(){
			if (isset($_POST['password'])) {
				if (self::is_autentified($_POST['password'])){
					$GLOBALS['generatedMenu'] = Menu::generate_menu();
					if (isset($_POST['hid_field']) && $_POST['hid_field'] != ''){
						header("Location: /" . $GLOBALS['site_dir'] . '/' . htmlentities($_POST['hid_field']));
						exit();
					}
					else {
						$this->view->generate('post_login_view.php', 'template_view.php', 'Authorization');
					}					
					
				}
				else {
					if (isset($_POST['hid_field'])) $this->data['referer'] = $_POST['hid_field'];
					$this->data['message'] = "<div class='neg-message'>Invalid password</div>";
					$this->view->generate('login_view.php', 'template_view.php', 'Authorization', $this->data);
				}
			}
		}	

		public function post_log_out(){
			if (isset($_POST['exit_hidden']) && $_POST['exit_hidden'] === 'true') {
				$this->model->destroy_session();
				$GLOBALS['generatedMenu'] = Menu::generate_menu();
				$this->view->generate('post_exit_view.php', 'template_view.php', 'Exit', $this->model->get_message());
			}
			else {
				$this->view->generate('post_exit_view.php', 'template_view.php', 'Exit', $this->model->get_message());
			}			
		}	
	}
?>