<?php
	class Controller 
	{		
		protected $view;		
		protected $session;
		
		public function __construct(){
			$this->view = new View();
		}
		
		public static function is_authorized(){
			return (isset($_SESSION['in']) && $_SESSION['in']);
		}
		
		public static function is_config_exist(){
			return is_file('config.php');
		}
		
		public static function is_autentified($pass){	
				require_once 'config.php';	
				return $_SESSION['in'] = (isset($admin_pass) && md5($pass) == $admin_pass);					
		}
	}
?>