<?php 
	class Menu
	{		
		public static function generate_menu(){
			$generatedMenu = array();
			global $main_menu;
			
			if (!isset($_SESSION)) session_start();			
			if (Controller::is_authorized()) {
				foreach ($main_menu as $key => $menu_item) {			
					if ($menu_item['access'] >= 0 ) {
						$generatedMenu[$menu_item['path']] = $menu_item['title'];
					}					
				}
			}				
			else {
				foreach ($main_menu as $key => $menu_item) {					
					if ($menu_item['access'] <= 0 ) {
						$generatedMenu[$menu_item['path']] = $menu_item['title'];
					}							
				}	
			}
			return $generatedMenu;
		}
	}
?>