<?php 
	class UserConfig
	{		
		private $host;
		private $login;
		private $password;
		private $db_name;
		private $admin_pass;
		
		function get_host(){
			return $this->host;
		}
		
		function set_host($value){
			$this->host = $value;
		}
		
		function get_login(){
			return $this->login;
		}
		
		function set_login($value){
			$this->login = $value;
		}
		
		function get_password(){
			return $this->password;
		}
		
		function set_password($value){
			$this->password = $value;
		}
		
		function get_db_name(){
			return $this->db_name;
		}
		
		function set_db_name($value){
			$this->db_name = $value;
		}
		
		function get_admin_pass(){
			return $this->admin_pass;
		}
		
		function set_admin_pass($value){
			$this->admin_pass = $value;
		}
	}
?>