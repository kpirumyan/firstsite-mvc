<?php 
	class View
	{
		public function generate($content_view, $template_view, $header, $data = null) {
			// rendering of content_view
			$content_view = $this->renderContent($content_view, $data);
			// rendering full view with content_view in it
			echo $this->renderView($template_view, $content_view, $header);			
		}	
		
		private function renderContent($content_view, $data = null){
			if(is_array($data))	extract($data);
			
			ob_start();
			require 'application/views/' . $content_view;
			return ob_get_clean();
		}
		
		private function renderView($template_view, $content_view, $header){
			ob_start();
			require 'application/views/' . $template_view;
			return ob_get_clean();
		}
	}
?>