<?php 
	class Model_Gallery
	{		
		private $cur_img;
		private $hidden_field;
		private $log;
		private $images = array();
		
		function __construct(){
			// create array of 'Image' directory files
			$this->set_images();	
			
			// define variables
			$this->get_data();	
		}
		
		public function get_cur_img(){
			return $this->cur_img;
		}
		
		public function get_log(){
			return $this->log;
		}
		
		public function get_images(){
			return $this->images;
		}
		
		private function set_images(){
			$dir = 'images';
			if (is_dir($dir))	{
				if ($dh = opendir($dir)) {
					while (($file = readdir($dh)) !== false) {
						if($file=='.' || $file=='..') continue;	
						$this->images[] = 'images/' .$file;
					}		
					closedir($dh); 
				}
			}
		}		
		
		private function remove_img(){
			try {
				if ($this->cur_img) {
					@unlink($this->cur_img);
					$this->log =  "Image removed";	
					$this->images = array();
					$this->set_images();
				}					
				else
					throw new Exception("Error on remove");
			}
			catch (Exception $e) {
				$this->log = $e->getMessage();
			}			
		}
		
		private function get_data(){	
			// get hidden elements values
			if (isset($_POST['hidden_img'])) 
				$this->cur_img = $_POST['hidden_img'];
			if (isset($_POST['hidden_field'])) 
				$this->hidden_field = $_POST['hidden_field'];		
				
			
			// paging images
			$this->cur_img = $this->paging_images();
		}		
		
		private function paging_images(){
			switch ($this->hidden_field) {
				case 'prev':
					$index = array_search($this->cur_img, $this->images);
					if ($index == 0) $index = count($this->images);
					return $this->images[$index - 1];var_dump($index);
				case 'next':
					$index = array_search($this->cur_img, $this->images);
					if ($index == count($this->images) - 1) $index = -1;
					return $this->images[$index + 1];print_r($index);
				case 'delete':
					$this->remove_img();
										
				// show default page
				default:
					if (count($this->images) > 0)	return $this->images[0];					
					else return null;
			}
		}
	}
?>