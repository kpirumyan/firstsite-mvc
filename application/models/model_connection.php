<?php 
	require_once "application/core/user_config.php";
	
	class Model_Connection
	{		
		protected $uc;
		protected $conn;
		
		function __construct(){
			$this->uc = new UserConfig();
			$this->read_config();
			
			try {
				$this->connect_to_db();
			}
			catch (PDOException $ex){
				die($ex->getMessage());
			}							
		}
		
		private function connect_to_db(){
			$this->conn = new PDO(
				"mysql:host={$this->uc->get_host()};dbname={$this->uc->get_db_name()}", 
				$this->uc->get_login(), 
				$this->uc->get_password()
			);
		}
		
		// Create config instance from config file 
		private function read_config(){
			require_once('config.php');
			$this->uc->set_host($host);
			$this->uc->set_login($login);
			$this->uc->set_password($password);
			$this->uc->set_db_name($db_name);
			$this->uc->set_admin_pass($admin_pass);	
		}	
	}
?>