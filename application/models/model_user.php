<?php 
	class Model_User 
	{		
		private $message = "The session continues";
		
		function get_message(){
			return $this->message;
		}
		function destroy_session() {
			$_SESSION = array();
			if (session_id() != "" || isset($_COOKIE[session_name()]))
				setcookie(session_name(), '', time()-2592000, '/');
			session_destroy();	
			$this->message = "The session clear";
		}
	}
?>