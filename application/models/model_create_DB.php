<?php 
	class Model_Create_DB
	{		
		public function get_form_data($uc){
			if (isset($_POST['host']) && isset($_POST['login']) && 
					isset($_POST['password']) && isset($_POST['db-name']) && isset($_POST['admin-pass'])) {
				$uc->set_host(htmlentities($_POST['host']));
				$uc->set_login(htmlentities($_POST['login']));
				$uc->set_password(htmlentities($_POST['password']));
				$uc->set_db_name(htmlentities($_POST['db-name']));
				$uc->set_admin_pass(htmlentities($_POST['admin-pass']));
			}				

			return $uc;
		}
		
		function create_db($uc){		
		
			$conn = mysqli_connect($uc->get_host(), $uc->get_login(), $uc->get_password());
			if (!$conn)	die('Could not connect: ' . mysql_error());		
			if (!mysqli_select_db($conn, $uc->get_db_name())) {
				$query = "CREATE DATABASE " .$uc->get_db_name();
				mysqli_query($conn, $query);
				mysqli_select_db($conn, $uc->get_db_name());
				$query = "CREATE TABLE Feedback(
					`ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
					`Name` VARCHAR(50) NOT NULL,
					`Email` VARCHAR(50) NOT NULL,
					`Post` text NOT NULL,
					`Date` date NOT NULL
				)";
				mysqli_query($conn, $query);
			}
			mysqli_close($conn);
		}		
		
		public function create_config_file($uc){
			$fd = fopen('config.php', 'w') or die("Open file error");
			fwrite($fd, "<?php\r\n");
			fwrite($fd, "\$host = '{$uc->get_host()}';\r\n");
			fwrite($fd, "\$login = '{$uc->get_login()}';\r\n");
			fwrite($fd, "\$password = '{$uc->get_password()}';\r\n");
			fwrite($fd, "\$db_name = '{$uc->get_db_name()}';\r\n");
			fwrite($fd, "\$admin_pass = '" . md5($uc->get_admin_pass()) . "';\r\n");
			fwrite($fd, "?>");
			fclose($fd);
		}			
	}
?>