<?php 
	require_once 'application/models/model_connection.php';
	
	class Feedback{}
	
	class Model_Feedback extends Model_Connection
	{		
		function insert($name, $email, $post){	
			$post_date = date('y/m/d');
			
			$sql = $this->conn->prepare("INSERT INTO feedback (Name, Email, Post, Date) VALUES (:name, :email, :post, :post_date)");
			$sql->bindParam(':name', $name);
			$sql->bindParam(':email', $email);
			$sql->bindParam(':post', $post);
			$sql->bindParam(':post_date', $post_date);
			$sql->execute();
			
			$this->conn = null;
			$sql = null;		

			return "<div class='pos-message'>Your feedback successfully submitted. Thank you.</div>";
		}
		
		function get_list(){
			$query = 'SELECT id, name, email FROM feedback';
			$sql = $this->conn->prepare($query);			
			$sql->execute();
			$this->conn = null;
			return $sql->fetchAll(PDO::FETCH_OBJ);			
		}
		
		function remove_post($id){
			$query = 'DELETE FROM feedback WHERE ID = :id';			
			$sql = $this->conn->prepare($query);			
			$sql->bindParam(':id', $id);
			$sql->execute();	
			$sql = null;
		}
		
		function get_post_byID($id){	
			$query = 'SELECT * FROM feedback WHERE ID = :id';
			$sql = $this->conn->prepare($query);		
			$sql->bindParam(':id', $id);			
			$sql->execute();
			$this->conn = null;
			$res =  $sql->fetch(PDO::FETCH_OBJ);
			return $res;	
		}
		
		// edit post
	}
?>