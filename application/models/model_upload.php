<?php 
	class Model_Upload
	{		
		private $status = array();
		
		public function get_status(){
			return $this->status;
		}
		
		// Image uploading errors
		function image_error($error){	
			switch ($error) {
				case UPLOAD_ERR_INI_SIZE: 
					$this->status[] = "The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
					break; 
				case UPLOAD_ERR_FORM_SIZE: 
					$this->status[] = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form"; 
					break; 
				case UPLOAD_ERR_PARTIAL: 
					$this->status[] = "The uploaded file was only partially uploaded"; 
					break; 
				case UPLOAD_ERR_NO_FILE: 
					$this->status[] = "No file was uploaded"; 
					break; 
				case UPLOAD_ERR_NO_TMP_DIR: 
					$this->status[] = "Missing a temporary folder"; 
					break; 
				case UPLOAD_ERR_CANT_WRITE: 
					$this->status[] = "Failed to write file to disk"; 
					break; 
				case UPLOAD_ERR_EXTENSION: 
					$this->status[] = "File upload stopped by extension"; 
					break; 

				default: 
					$this->status[] = "Image added successfully"; 
					break; 
			} 	
		}
		
		function upload_images($files){			
			foreach ($files["uploads"]["error"] as $key => $error) {
				if ($error == UPLOAD_ERR_OK) {
					$tmp_name = $files["uploads"]["tmp_name"][$key];
					$name = "Images/" . $files["uploads"]["name"][$key];
					move_uploaded_file($tmp_name, $name);
				}				
			}				
		}
	}
?>